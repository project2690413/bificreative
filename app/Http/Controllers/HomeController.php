<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $data['title'] = "PT Bifi Creative";
        return view('front.index', $data);
    }
    public function portal_dm()
    {
        $data['title'] = "Digital Marketing";
        $data['judul'] = "Digital Marketing";
        $data['sub'] = "Digital Marketing";
        $data['bg_hero'] = "img/hero/digmar.jpg";
        return view('front.services.digitalmarketing.index', $data);
    }
    public function portal_jvp()
    {
        $data['title'] = "Jasa Edit Video Promosi";
        $data['judul'] = "Edit Video Promosi";
        $data['sub'] = "Edit Video Promosi";
        $data['bg_hero'] = "img/hero/promvid.jpg";
        return view('front.services.promosivideo.index', $data);
    }
    public function portal_wd()
    {
        $data['title'] = "Web Development";
        $data['judul'] = "Web Development";
        $data['sub'] = "Web Development";
        $data['bg_hero'] = "img/hero/webdev.jpg";
        return view('front.services.webdevelopment.index', $data);
    }

    public function about()
    {
        $data['title'] = "About";
        $data['judul'] = "About";
        $data['sub'] = "About";
        $data['bg_hero'] = "img/hero/about.jpg";
        return view('front.about.index', $data);
    }
}
