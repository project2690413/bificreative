@extends('template.main')

@section('konten')
    <main>
        <!-- ======= Hero Section ======= -->
        <section id="hero" class="hero d-flex align-items-center" style="background-image: url(img/hero/pict1.jpeg)">
            <div class="container">
                <div class="row gy-4 d-flex justify-content-between">
                    <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center p-content">
                        <h2 data-aos="fade-up">Bifi Creative</h2>
                        <p data-aos="fade-up" data-aos-delay="15">Lorem ipsum dolor sit amet consectetur adipisicing
                            elit.
                            Voluptatem adipisci neque expedita ab! Amet molestiae, tempore expedita minima illum
                            accusamus,
                            excepturi laborum temporibus qui libero beatae, facilis placeat exercitationem recusandae
                            deserunt. Dolor voluptas, repellendus nemo, aliquam deleniti temporibus nobis ea in dolores
                            pariatur eligendi commodi veniam voluptatem sed dolorem assumenda?</p>
                        <div class="row gy-4" data-aos="fade-up" data-aos-delay="50">
                            <a href="/about">
                                <button class="cssbuttons-io">
                                    <span>Pelajari Selengkapnya</span>
                                </button>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-6 order-1 order-lg-2 hero-img text-center" data-aos="zoom-out">
                        {{-- <img src="assets/img/about.jpg" class="img-fluid" alt="">
                        <a href="https://www.youtube.com/embed/kR_wXpChitQ" class="glightbox play-btn"></a> --}}
                        <iframe class="youtube" width="460" height="315"
                            src="https://www.youtube.com/embed/kR_wXpChitQ?autoplay=1&mute=1" frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section><!-- End Hero Section -->

        <section id="services" class="services d-flex align-items-center">
            <div class="container">
                <h3 data-aos="fade-up" class="text-center fw-bold">Services</h3>
                <div data-aos="fade-up" data-aos-delay="30" class="row gy-4 d-flex justify-content-between service-margin">
                    <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center p-content">
                        <h3 data-aos="fade-up">1. Digital Marketing</h3>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit.
                            Quasi quos consequatur officiis
                            laboriosam, commodi optio, nobis aperiam eos rerum maiores eaque voluptatibus veniam
                            voluptates
                            voluptatem quidem laborum! Voluptatibus rerum dicta officiis cum! Nostrum error accusantium
                            quam
                            maxime provident voluptatum itaque iure quia ratione. Officiis sunt, error id distinctio
                            labore
                            expedita!</p>
                        <a href="/digital_marketing">
                            <button class="cssbuttons-io" style="border:2px solid #27487a;">
                                <span>Pelajari Selengkapnya</span>
                            </button>
                        </a>
                    </div>
                    <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-out">
                        <img src="img/assets/pict1.png" class="assets-img">
                    </div>
                </div>
                <div data-aos="fade-up" data-aos-delay="40" class="row gy-4 d-flex justify-content-between service-margin">
                    <div class="col-lg-6 order-2 order-lg-2 d-flex flex-column justify-content-center p-content">
                        <h3 data-aos="fade-up">2. Jasa Video Promosi</h3>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit.
                            Quasi quos consequatur officiis
                            laboriosam, commodi optio, nobis aperiam eos rerum maiores eaque voluptatibus veniam
                            voluptates
                            voluptatem quidem laborum! Voluptatibus rerum dicta officiis cum! Nostrum error accusantium
                            quam
                            maxime provident voluptatum itaque iure quia ratione. Officiis sunt, error id distinctio
                            labore
                            expedita!</p>
                        <a href="/jasa_video_promosi">
                            <button class="cssbuttons-io" style="border:2px solid #27487a;">
                                <span>Pelajari Selengkapnya</span>
                            </button>
                        </a>
                    </div>
                    <div class="col-lg-6 order-1 order-lg-1 hero-img" data-aos="zoom-out">
                        <img src="img/assets/pict2.png" class="assets-img">
                    </div>
                </div>
                <div data-aos="fade-up" data-aos-delay="40" class="row gy-4 d-flex justify-content-between service-margin">
                    <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center p-content">
                        <h3>3. Web Development</h3>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit.
                            Quasi quos consequatur officiis
                            laboriosam, commodi optio, nobis aperiam eos rerum maiores eaque voluptatibus veniam
                            voluptates
                            voluptatem quidem laborum! Voluptatibus rerum dicta officiis cum! Nostrum error accusantium
                            quam
                            maxime provident voluptatum itaque iure quia ratione. Officiis sunt, error id distinctio
                            labore
                            expedita!</p>
                        <a href="/web_development">
                            <button class="cssbuttons-io" style="border:2px solid #27487a;">
                                <span>Pelajari Selengkapnya</span>
                            </button>
                        </a>
                    </div>
                    <div class="col-lg-6 order-1 order-lg-2 hero-img " data-aos="zoom-out">
                        <img src="img/assets/pict3.png" class="assets-img">
                    </div>
                </div>
            </div>
        </section><!-- End Hero Section -->

        <div style="position: relative; top:10px;">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path fill="#0e1d34" fill-opacity="1"
                    d="M0,224L80,213.3C160,203,320,181,480,192C640,203,800,245,960,250.7C1120,256,1280,224,1360,208L1440,192L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z">
                </path>
            </svg>
        </div>

        <section id="why" class="reason d-flex align-items-center">
            <div class="container">
                <h3 class="text-center fw-bold" data-aos="fade-up">Mengapa Harus BifiCreative?</h3>
                <div class="row mt-5 gy-4 d-flex justify-content-between text-center">
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="70">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="70">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="70">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="50">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                </div>
                <div class="row mt-5 gy-4 d-flex justify-content-between text-center">
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="50">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="50">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="50">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                    <div class="col-lg-3 col-6 p-3 d-flex flex-column justify-content-center" data-aos="fade-up"
                        data-aos-delay="50">
                        <img class="mb-3" src="img/assets/pict2.png">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, rerum.</p>
                    </div>
                </div>
            </div>
        </section><!-- End Hero Section -->
        <div style="position: relative; bottom:30px;">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path fill="#0e1d34" fill-opacity="1"
                    d="M0,288L80,250.7C160,213,320,139,480,122.7C640,107,800,149,960,165.3C1120,181,1280,171,1360,165.3L1440,160L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z">
                </path>
            </svg>
        </div>

        <section id="testi" class="feedback d-flex">
            <div class="container">
                <h3 class="text-center fw-bold" data-aos="fade-up">Feedback Customer</h3>
                {{-- <div data-aos="fade-up" class="row gy-4 d-flex justify-content-between mt-5">
                    <div class="col-lg-4 order-2 order-lg-1 d-flex flex-column justify-content-center">
                        <div class="card" style="width: 18rem;">
                            <img src="img/logo/bifi_creative_logo.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 order-2 order-lg-1 d-flex flex-column justify-content-center">
                        <div class="card" style="width: 18rem;">
                            <img src="img/logo/bifi_creative_logo.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 order-2 order-lg-1 d-flex flex-column justify-content-center">
                        <div class="card" style="width: 18rem;">
                            <img src="img/logo/bifi_creative_logo.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </section><!-- End Hero Section -->
    </main>
@endsection
