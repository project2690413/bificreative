<!-- ======= Header ======= -->
<header id="header" class="header d-flex align-items-center fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
        <a href="/" class="logo d-flex align-items-center">
            <img src="img/logo/bifi_creative_logo.png" class="brand-logo">
        </a>
        <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
        <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
        <nav id="navbar" class="navbar">
            <ul>
                {{-- <li><a href="#hero" class="active">Home</a></li> --}}
                <li><a href="/">Home</a></li>
                <li><a href="contact.html">Contact</a></li>
                <li><a class="get-a-quote" href="get-a-quote.html">Get a Contact</a></li>
            </ul>
        </nav><!-- .navbar -->
    </div>
</header><!-- End Header -->
<!-- End Header -->
